# Game 15
Browser game.
## Build
You have install typescript compiler.
```sh
$ git clone https://bitbucket.org/michmv/game15.git
$ cd game15
$ npm update
$ sh build.sh
```
## Release
[Release](https://bitbucket.org/michmv/game15/src/release/)
[Demo](http://mmv-module.herokuapp.com/demos/game15/)
## Use
Open in browser `index.html`.
You have internet connection for load JQuery and Require library from [cdnjs.com](https://cdnjs.com/).
## License
MIT
