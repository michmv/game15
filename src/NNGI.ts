/**
 * Noob Node Graphic Interface
 * 
 * @version 2.1.0
 * @see <url>
 * @dependence: jQuery
 * 
 * Simple GI
 * 
 * @
 * import * as NNGI from "./NNGI"
 *
 * interface App {
 * }
 *
 * let widget: NNGI.Container<App> = new NNGI.Container()
 * widget.children.push(new NNGI.Tag('div', {text: "Hello World!"}))
 *
 * let app: App = {}
 *
 * $(document).ready(function() {
 *     widget.build(app, undefined, $('body'))
 * 
 *     $(window).resize( () => NNGI.eventWidgetUp(widget, new NNGI.Resize) )
 * });
 * @
 */

export interface Draw {
    it: JQuery<HTMLElement>,
    container: JQuery<HTMLElement>
}

/**
 * Foundation for custom widget
 */
export abstract class Widget<T>
{
    __index: number
    state: T
    parent: Widget<T>
    children: Array<Widget<T>> = []
    it: JQuery<HTMLElement>
    container: JQuery<HTMLElement>
    buildStatus: boolean = false

    build(state: T, parent: Widget<T>, target: JQuery<HTMLElement>): boolean {
        let result = false

        if(!this.buildStatus) {
            this.parent = parent
            this.state = state
            let response =  this.draw(target)
            this.it = response.it
            this.container = response.container
            result = true
        }

        for(let i=0; i<this.children.length; i++) {
            if(this.children[i].build(state, this, this.container)) result = true
        }

        if(result) {
            this.afterDraw()
            this.buildStatus = true
        }

        return result
    }

    buildChildren() {
        if(this.state && this.container) {
            for(let i=0; i<this.children.length; i++) {
                this.children[i].build(this.state, this, this.container)
            }
        }
    }

    abstract draw(target: JQuery<HTMLElement>): Draw

    afterDraw(): void {}

    event(event: EventWidget): EventWidget { return event }

    /**
     * Add child in this node and call build(...) for it
     */
    append(widget: Widget<T>): void {
        this.children.push(widget)
        widget.build(this.state, this, this.container)
    }

    empty(): void {
        for(let i=0; i<this.children.length; i++) {
            this.children[i].remove()
        }
        this.children = []
    }

    remove(): void {
        this.it.remove()
        this.children = []
    }

    /**
     * If this is root of tree to remove html, but not remove this node
     */
    removeExtra(): void {
        this.remove()

        if(this.parent !== undefined) {
            this.parent.reindex()
            this.parent.removeChildrenByIndex(this.__index)
        }
    }

    reindex(): void {
        for(let i=0; i<this.children.length; i++) {
            this.children[i].__index = i
        }
    }

    removeChildrenByIndex(n: number): void {
        this.children = this.children.filter(((x: Widget<T>) => x.__index != this.__index), this)
    }

    /**
     * Create event in widget and children
     */
    eventWidgetUp(event: EventWidget): EventWidget {
        let response: EventWidget

        response = this.event(event)

        let i = 0
        while(response.stop !== true && i < this.children.length) {
            response = this.children[i].eventWidgetUp(response)
            i++
        }

        return response
    }

    /**
     * Create event in widget and parents
     */
    eventWidgetDown(event: EventWidget): EventWidget {
        let response: EventWidget

        response = this.event(event)

        let t: Widget<T> = this
        while(response.stop !== true && t.parent !== undefined) {
            t = t.parent
            response = t.event(response)
        }

        return response
    }

    findRoot(): Widget<T> {
        let t: Widget<T> = this

        while(t.parent !== undefined) {
            t = t.parent
        }

        return t
    }

    findParents(): Array<Widget<T>>
    {
        let result = []
        let t: Widget<T> = this

        while(t.parent !== undefined) {
            t = t.parent
            result.push(t)
        }

        return result
    }

    findWidgetUp(fn: (x: Widget<T>) => boolean): Widget<T> | false {
        let result: Widget<T> | false = false

        if(fn(this)) {
            return this
        } else {
            for(let i=0; i<this.children.length; i++) {
                result = this.children[i].findWidgetUp(fn)
                if(result !== false) break
            }
        }

        return result
    }

    findWidgetUpAll(fn: (x: Widget<T>) => boolean): Array<Widget<T>> {
        let result = []

        if(fn(this)) result.push(this)

        for(let i=0; i<this.children.length; i++) {
            result.concat(this.children[i].findWidgetUpAll(fn))
        }

        return result
    }

    findWidgetDown(fn: (x: Widget<T>) => boolean): Widget<T> | false {
        let result: Widget<T> | false = false

        if(fn(this)) {
            return this
        } else {
            let t: Widget<T> = this
            while(t.parent !== undefined) {
                if(fn(t.parent)) return t.parent
                t = t.parent
            }
        }

        return result
    }

    findWidgetDownAll(fn: (x: Widget<T>) => boolean): Array<Widget<T>> {
        let result = []

        if(fn(this)) result.push(this)

        if(this.parent !== undefined) {
            result.concat(this.parent.findWidgetDownAll(fn))
        }

        return result
    }
}

/**
 * Foundation for event in widget
 */
export abstract class EventWidget {
    stop: boolean // If stop = true to stop event

    constructor() { this.stop = false }
}

export class Resize extends EventWidget {}

/**
 * Empty Widget for root tree
 */
export class Main<T> extends Widget<T>
{
    draw(target: JQuery<HTMLElement>) {
        return {it: target, container: target}
    }
}

/**
 * Simple widget
 */
export class Tag<T> extends Widget<T>
{
    constructor(public tag: string = "div", public options = {}) {
        super();
    }

    draw(target: JQuery<HTMLElement>)
    {
        let element = $(`<${this.tag}>`, this.options)
        target.append(element)
        return { it: element, container: element}
    }
}