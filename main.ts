///<reference path="./node_modules/@types/jquery/index.d.ts" />

import * as S from "./src/App"

let app: S.App = {
    field: S.random15(S.start),
    win: false
}

let field  = new S.Field
for(let i=0; i<app.field.length; i++) {
    field.children.push(new S.Chip(app.field[i]))
}

let widget: S.Main = new S.Main;
widget.children.push(field)
widget.children.push(new S.WinMessage)

$(document).ready(function() {
    widget.build(app, undefined, $('body'))
    widget.eventWidgetUp(new S.InitPosition)

    $(window).resize( () => widget.eventWidgetUp(new S.Resize) )

    $(document).keydown((e) => {
        switch(e.which) {
            case 38: widget.eventWidgetUp(new S.MoveByKey(S.KeyMove.UP)); break
            case 39: widget.eventWidgetUp(new S.MoveByKey(S.KeyMove.RIGHT)); break
            case 40: widget.eventWidgetUp(new S.MoveByKey(S.KeyMove.DOWN)); break
            case 37: widget.eventWidgetUp(new S.MoveByKey(S.KeyMove.LEFT)); break
        }
    })
});