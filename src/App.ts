import * as NNGI from "./NNGI"

export const start = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0]

export const sizeField = 370

export interface App {
    field: Array<number>,
    win: boolean
}

export type Move = false | Array<number>

export function compare<T>(a1: Array<T>, a2: Array<T>): boolean {
    return a1.length == a2.length && a1.every((v,i)=>v === a2[i])
}

export function findIndex<T>(list: Array<T>, func: (e: T) => boolean): number {
    for(let i=0; i<list.length; i++) {
        if(func(list[i])) return i
    }

    return -1
}

export function find<T>(list: Array<T>, func: (e: T) => boolean): undefined | T {
    for(let i=0; i<list.length; i++) {
        if(func(list[i])) return list[i]
    }

    return undefined
}

export function getRandomInt(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function random15(fieldStart: Array<number>): Array<number> {
    let n = 500
    let can_move: Array<number>
    let rand: number
    let t: Move
    let field = fieldStart.slice()

    while(n > 0) {
        can_move = canMoveIndex(field)
        rand = getRandomInt(0, can_move.length - 1)
        t = moveByIndex(field, can_move[rand])
        if(t !== false) {
            field = t
        } else {
            return field
        }
        n--
    }

    return field
}

export function canMoveIndex(field: Array<number>): Array<number> {
    let result = []
    let zero: number = findIndex(field, (e) => e == 0 )

    if(zero - 4 >= 0) result.push(zero - 4)
    if(zero + 4 <= 15) result.push(zero + 4)
    if(zero > 0 && zero % 4 != 0) result.push(zero - 1)
    if(zero < 15 && zero % 4 != 3) result.push(zero + 1)

    return result
}

export function moveByIndex(field: Array<number>, index: number): Move {
    let can_move = canMoveIndex(field)

    let t = find(can_move, (e) => e == index)

    if(t !== undefined) {
        let zero = findIndex(field, (e) => e == 0)
        field[zero] = field[index]
        field[index] = 0
        return field
    } else {
        return false
    }
}

export function moveByNumber(field: Array<number>, n: number): Move {
    let index = findIndex(field, (e) => e == n)
    return moveByIndex(field, index)
}

export enum KeyMove { UP, RIGHT, DOWN, LEFT };

export function moveByButton(field: Array<number>, key: KeyMove): Move {
    let result: Move = false
    let zero: number = findIndex(field, (e) => e == 0)

    if     (key == KeyMove.UP)    result = moveByIndex(field, zero + 4)
    else if(key == KeyMove.DOWN)  result = moveByIndex(field, zero - 4)
    else if(key == KeyMove.LEFT)  result = moveByIndex(field, zero + 1)
    else if(key == KeyMove.RIGHT) result = moveByIndex(field, zero - 1)

    return result
}

export class Main extends NNGI.Widget<App>
{
    draw(target: JQuery<HTMLElement>) {
        let element = $('<div>', {id: "main"})
        this.size(element)
        target.append(element)

        return {it: element, container: element}
    }

    size(element: JQuery<HTMLElement>) {
        element.width( $(window).width() )
        element.height( $(window).height() )
    }

    event(event: NNGI.EventWidget) {
        if(event instanceof Resize) {
            this.size(this.it)
        }
        return event
    }
}

export class Field extends NNGI.Widget<App>
{
    draw(target: JQuery<HTMLElement>) {
        let element = $('<div>', {class: 'field'})
        element.css({width: sizeField, height: sizeField})
        target.append(element)
        return {it: element, container: element}
    }

    afterDraw() {
        let w = this.parent.it.width()
        let h = this.parent.it.height()

        let wIt = this.it.width()
        let hIt = this.it.height()

        this.it.css({
            left: w / 2 - wIt / 2,
            top: h / 2 - hIt / 2
        })
    }

    event(event: NNGI.EventWidget) {
        if(event instanceof Resize) {
            this.afterDraw()
        }
        if(event instanceof MoveByNumber) { // click mouse button
            if(!this.state.win) {
                this.moveChip(moveByNumber(this.state.field, event.value))
            }
            event.stop = true
        }
        if(event instanceof MoveByKey) { // press key
            if(!this.state.win) {
                this.moveChip(moveByButton(this.state.field, event.key))
            }
            event.stop = true
        }
        return event
    }

    moveChip(move: Move) {
        if(move !== false) {
            this.state.field = move
            this.eventWidgetUp(new NewPosition)

            if(compare(start, move)) this.findRoot().eventWidgetUp(new Win)
        }
    }
}

export class WinMessage extends Field
{
    draw(target: JQuery<HTMLElement>) {
        let element = $('<div>', {class: 'win', text: "You win!!! Press F5 for new game."})
        target.append(element)
        return {it: element, container: element}
    }

    event(event: NNGI.EventWidget) {
        if(event instanceof Resize) {
            this.afterDraw()
        }

        if(event instanceof Win) {
            this.it.show()
            this.state.win = true
            event.stop = true
        }

        return event
    }
}

export class Chip extends NNGI.Widget<App>
{
    constructor(public value: number) { super() }

    setPosition(init: boolean = false) {
        let duration = 200
        if(init) duration = 0
        for(let i=0; i<this.state.field.length; i++) {
            let item = this.state.field[i]
            if(item == this.value) {
                let y = Math.floor( i / 4 )
                let x = i % 4

                this.it.animate({left: 10 + x*80 + 10*x, top: 10 + y*80 + 10*y}, duration)

                break
            }
        }
    }

    draw(target: JQuery<HTMLElement>) {
        let element = $('<div>', {class: 'chip'})
        element.css({top: -2000})

        if(this.value == 0) {
            element.addClass('zero')
        } else {
            element.append($('<div>', {class: 'sub', text: this.value}))
            element.on('click', (() => {
                this.eventWidgetDown(new MoveByNumber(this.value))
            }).bind(this) )
        }

        target.append(element)

        return {it: element, container: element}
    }

    event(event: NNGI.EventWidget) {
        if(event instanceof NewPosition) {
            this.setPosition()
        }

        if(event instanceof InitPosition) {
            this.setPosition(true)
        }

        return event
    }
}

export class Resize extends NNGI.EventWidget {}

export class NewPosition extends NNGI.EventWidget {}

export class InitPosition extends NNGI.EventWidget {}

export class Win extends NNGI.EventWidget {}

export class MoveByNumber extends NNGI.EventWidget {
    constructor(public value: number) { super() }
}

export class MoveByKey extends NNGI.EventWidget {
    constructor(public key: KeyMove) { super() }
}